from rest_framework import serializers

from lessons.models import LessonModel

class LessonSerializer(serializers.ModelSerializer):
    class Meta:
        model = LessonModel
        fields = ('id', 'lesson_name', 'lesson_credit')
