from django.urls import path, include
from rest_framework import routers

from lessons.views import LessonViewSet

app_name = 'lessons'

router = routers.SimpleRouter()
router.register('', LessonViewSet)

urlpatterns = [
    path('', include(router.urls)),
]
