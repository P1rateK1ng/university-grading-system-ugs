from django.db import models

from utils.utils import BaseModel


class LessonModel(BaseModel):
    lesson_name = models.CharField(max_length=100, verbose_name='Название урока')
    lesson_credit = models.IntegerField(verbose_name='Кредит урока')

    class Meta:
        verbose_name = 'Урок'
        verbose_name_plural = 'уроки'

    def __str__(self):
        return self.lesson_name
