from rest_framework import viewsets

from lessons.models import LessonModel
from lessons.serializers import LessonSerializer
from utils.permissions import IsAdminOrReadOnly


class LessonViewSet(viewsets.ModelViewSet):
    """
        The lessons can be created by admins and only. The list of lessons can be viewed by students.
    """
    queryset = LessonModel.objects.all()
    serializer_class = LessonSerializer
    permission_classes = (IsAdminOrReadOnly,)
