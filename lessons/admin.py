from django.contrib import admin

from lessons.models import LessonModel

@admin.register(LessonModel)
class LessonAdmin(admin.ModelAdmin):
    list_display = ('lesson_name', 'lesson_credit')
    search_fields = ('lesson_name', 'lesson_credit')
