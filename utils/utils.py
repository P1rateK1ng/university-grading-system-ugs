import uuid
from django.db import models
from django.contrib.auth.models import AbstractUser

# BaseModel for any models in this project. Thus, I am not going to write these fields in every model.
class BaseModel(models.Model):
    id = models.UUIDField(default=uuid.uuid4, editable=False, primary_key=True)
    created_at = models.DateTimeField(auto_now_add=True, verbose_name='Дата создания')
    updated_at = models.DateTimeField(auto_now=True, verbose_name='Дата обновления')

    class Meta:
        abstract = True

# BaseUserModel, has the same purpose as a BaseModel. Also, it inherited from AbsractUser in django.
class BaseUserModel(AbstractUser, BaseModel):
    first_name = models.CharField(max_length=30, verbose_name='Имя')
    last_name = models.CharField(max_length=30, verbose_name='Фамилия')
    email = models.EmailField(max_length=254, verbose_name='Почта', unique=True)

    class Meta:
        abstract = True
