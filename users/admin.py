from django.contrib import admin

from users.models import UserModel, UserRolesModel


@admin.register(UserModel)
class UserAdmin(admin.ModelAdmin):
    list_display = ('last_name', 'first_name', 'role',)
    search_fields = ('id', 'last_name', 'first_name', 'role',)
    exclude = ('password',)

@admin.register(UserRolesModel)
class UserRolesAdmin(admin.ModelAdmin):
    list_display = ('title',)
    search_fields = ('title',)
