from django.urls import path, include
from rest_framework import routers
from .views import UserRegisterViewSet, UserViewSet, UserRolesViewSet
from rest_framework_simplejwt.views import TokenRefreshView, TokenObtainPairView

app_name = 'user'

router = routers.SimpleRouter()
router.register('role', UserRolesViewSet, basename='role')

urlpatterns = [
    # Endpoint to register new users:
    path('register/', UserRegisterViewSet.as_view(), name='user_register_endpoint'),
    path('list/', UserViewSet.as_view({'get': 'list'}), name='user_list_endpoint'),

    # JWT token obtain and refresh endpoints:
    path('login/', TokenObtainPairView.as_view(), name='token_obtain_pair'),
    path('token/refresh/', TokenRefreshView.as_view(), name='token_refresh'),

    # Roles CRUD:
    path('', include(router.urls), name='role')
]
