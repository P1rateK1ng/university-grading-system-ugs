from rest_framework import viewsets, generics, status
from rest_framework.response import Response
from rest_framework.permissions import IsAdminUser, AllowAny

from users.models import UserModel, UserRolesModel
from users.serializers import UserSerializer, UserRegisterSerializer, UserRolesSerializer


class UserViewSet(viewsets.ReadOnlyModelViewSet):
    """
        Only admin user can see the list of teachers. But doesn't have access to update the info of users.
    """
    queryset = UserModel.objects.all()
    serializer_class = UserSerializer
    permission_classes = (IsAdminUser,)

class UserRegisterViewSet(generics.CreateAPIView):
    """
        Here user can register. After registering through this endpoint, user must log in.
    """
    queryset = UserModel.objects.all()
    serializer_class = UserRegisterSerializer
    permission_classes = (AllowAny,)

    def post(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        user = serializer.save()
        data = {
            'email': user.email,
            'continue': 'http://127.0.0.1:8000/api/users/login/'
        }
        return Response(data, status=status.HTTP_201_CREATED)

class UserRolesViewSet(viewsets.ModelViewSet):
    queryset = UserRolesModel.objects.all()
    serializer_class = UserRolesSerializer
    permission_classes = (IsAdminUser,)
