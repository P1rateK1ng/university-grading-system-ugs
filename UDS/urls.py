from django.contrib import admin
from django.urls import path, include
from rest_framework import permissions
from drf_yasg.views import get_schema_view
from drf_yasg import openapi

schema_view = get_schema_view(
   openapi.Info(
      title="University Grading System [UGS]",
      default_version='v1',
      description="For now this project still in development state, stay tuned for more.",
   ),
   public=True,
   permission_classes=[permissions.AllowAny],
)

urlpatterns = [
    # Admin path:
    path('admin/', admin.site.urls),
    path("debug/", include("debug_toolbar.urls")),

    # My apps:
    path('api/users/', include('users.urls'), name='users-app'),
    path('api/lessons/', include('lessons.urls'), name='lessons-app'),
    path('api/grades/', include('grades.urls'), name='grades-app'),

    # Swagger documentation:
    path('swagger(P<format>\.json|\.yaml)', schema_view.without_ui(cache_timeout=0), name='schema-json'),
    path('swagger/', schema_view.with_ui('swagger', cache_timeout=0), name='schema-swagger-ui'),
    path('redoc/', schema_view.with_ui('redoc', cache_timeout=0), name='schema-redoc'),
]
