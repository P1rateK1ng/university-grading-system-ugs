from django.urls import path, include
from rest_framework.routers import SimpleRouter

from grades.views import GradeViewSet

router = SimpleRouter()
router.register('', GradeViewSet)

urlpatterns = [
    path('', include(router.urls))
]

