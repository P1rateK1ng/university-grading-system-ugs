from rest_framework import viewsets, permissions

from grades.models import GradeModel
from grades.serializer import GradeSerializer


class GradeViewSet(viewsets.ModelViewSet):
    queryset = GradeModel.objects.all()
    serializer_class = GradeSerializer
    permission_classes = (permissions.AllowAny,)
