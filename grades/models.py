from django.db import models

from lessons.models import LessonModel
from users.models import UserModel
from utils.utils import BaseModel


class GradeModel(BaseModel):
    lesson_id = models.ForeignKey(LessonModel, on_delete=models.CASCADE)
    student_id = models.ForeignKey(UserModel, on_delete=models.CASCADE)
    grade = models.IntegerField(null=True)

    class Meta:
        verbose_name = 'Оценка'
        verbose_name_plural = 'Оценки'

    def __str__(self):
        return self.grade
