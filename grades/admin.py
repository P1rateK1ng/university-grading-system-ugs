from django.contrib import admin

from grades.models import GradeModel


@admin.register(GradeModel)
class GradeAdmin(admin.ModelAdmin):
    list_display = ('student_id', 'lesson_id', 'grade')
    search_fields = ('student_id', 'lesson_id', 'grade')
